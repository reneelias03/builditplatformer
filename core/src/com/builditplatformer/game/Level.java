package com.builditplatformer.game;

import box2dLight.DirectionalLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;

/**
 * Created by rene__000 on 7/2/2015.
 */
class Level {

    int gridSize;
    Body ground;
    FixtureDef groundDef;
    BodyDef groundBodyDef;
    World world;
    Sprite gridSprite;
    Sprite platformSprite;
    float scale;
    AssetManager assetManager;
    SpriteBatch staticBatch;
    Vector2 viewport;
    float viewportDifference;
    ArrayList<Rectangle> gridBoxes;
    boolean currentlyPressed;
    boolean previousPressed;
    FreeTypeFontGenerator ftfGen;
    FreeTypeFontGenerator.FreeTypeFontParameter ftfpar;
    BitmapFont font;
    Vector2 textLocation;
    Vector2 rowCol;
    int selectedIndex;
    GameButtons playButton, leftButton, rightButton, leftJumpButton, rightJumpButton;
    ArrayList<PlatformBlock> platformBlocks;
    int allotedBlocks;
    RayHandler rayHandler;
    DirectionalLight sun;
    Hero hero;
    boolean showGrid;

    public Level(World world, int gridSize, int allotedBlocks, OrthographicCamera camera, float scale, AssetManager assetManager){
        this.gridSize = gridSize;

        PolygonShape groundShape = new PolygonShape();
        groundShape.setAsBox(8.5f * scale, 2f * scale);

        float worldScale = 1280f / Gdx.graphics.getWidth();

        groundDef = new FixtureDef();
        groundDef.friction = .6f;
        groundDef.restitution = 0f;
        groundDef.density = .2f;
        groundDef.shape = groundShape;

        groundBodyDef = new BodyDef();
        groundBodyDef.type = BodyDef.BodyType.StaticBody;
        groundBodyDef.position.x = camera.position.x;
        groundBodyDef.position.y = camera.position.y - 5.7f * scale;

        ground = world.createBody(groundBodyDef);
        ground.createFixture(groundDef);
        groundShape.dispose();

        this.world = world;
        this.scale = scale;
        this.assetManager = assetManager;
        LoadAssets();

        gridSprite = new Sprite(GetTextureAsset("BorderedBox.png"));

        viewport = new Vector2(1640f * scale, 800f * scale);
        viewportDifference = Gdx.graphics.getWidth() - viewport.x;

        staticBatch = new SpriteBatch();

        gridBoxes = new ArrayList<Rectangle>();

        int xIndex = 0;
        for(float x = viewportDifference / 2f; x < viewportDifference / 2f  + viewport.x; x += viewport.x / gridSize) {
            for(float y = 170f * scale; y < viewport.y; y += viewport.x / gridSize) {
                gridBoxes.add(new Rectangle(x, y, viewport.x / gridSize, viewport.x / gridSize));
            }
            xIndex++;
            if(xIndex == 16) {
                break;
            }
        }

        ftfGen = new FreeTypeFontGenerator(Gdx.files.internal("goodtimesrg.ttf"));
        ftfpar = new FreeTypeFontGenerator.FreeTypeFontParameter();
        ftfpar.size = 40;
        font = ftfGen.generateFont(ftfpar);
        textLocation = new Vector2(-1, -1);
        rowCol = new Vector2();
        selectedIndex = -1;

        playButton = new GameButtons(GetTextureAsset("PlayButton.png"), true, new Vector2(20f * scale, Gdx.graphics.getHeight() - 80f * scale));
        playButton.setSize(60f * scale, 60f * scale);
        playButton.setColor(Color.GREEN);

        leftButton = new GameButtons(GetTextureAsset("LeftRightButton.png"), true, new Vector2(20f * scale, 20f * scale));
        leftButton.setSize(150f * scale, 150f * scale);
        leftButton.setFlip(true, false);
//        leftButton.setColor(Color.LIGHT_GRAY);

        rightButton = new GameButtons(GetTextureAsset("LeftRightButton.png"), true, new Vector2(Gdx.graphics.getWidth() - 170f * scale, 20f * scale));
        rightButton.setSize(150f * scale, 150f * scale);
//        rightButton.setColor(Color.LIGHT_GRAY);

        leftJumpButton = new GameButtons(GetTextureAsset("JumpButton.png"), true, new Vector2(leftButton.getX() + leftButton.getWidth() + 20f * scale, leftButton.getY()));
        leftJumpButton.setSize(150f * scale, 150f * scale);

        rightJumpButton = new GameButtons(GetTextureAsset("JumpButton.png"), true, new Vector2(rightButton.getX() - rightButton.getWidth() - 20f * scale, rightButton.getY()));
        rightJumpButton.setSize(150f * scale, 150f * scale);

        this.allotedBlocks = allotedBlocks;
        platformBlocks = new ArrayList<PlatformBlock>(allotedBlocks);
        for(int i = 0; i < allotedBlocks; i++) {
            platformBlocks.add(new PlatformBlock(world, gridBoxes.get(0), camera, GetTextureAsset("SinglePlatform.png"), scale));
            platformBlocks.get(i).platformBody.setTransform(-1000, 0, 0);
            platformBlocks.get(i).platformButton.setPosition(-1000, 0);
        }


        rayHandler = new RayHandler(world);
        rayHandler.setAmbientLight(1, 1, 1, .28f);
        sun = new DirectionalLight(rayHandler, 1000, new Color(1 , 1, 1, .5f), -90);

        platformSprite = new Sprite(GetTextureAsset("Platform.png"));
        platformSprite.setSize((17f * scale) / camera.zoom, (2f * scale)/ camera.zoom);
        platformSprite.setPosition((ground.getPosition().x - camera.position.x + Gdx.graphics.getWidth() * camera.zoom / 2f) / camera.zoom - platformSprite.getWidth() / 2f,
                (ground.getPosition().y - camera.position.y + Gdx.graphics.getHeight() * camera.zoom / 2f) / camera.zoom);

        hero = new Hero(GetTextureAsset("Hero.png"), gridBoxes.get(0), world, camera, scale);
        showGrid = true;
    }

    private void LoadAssets() {
        assetManager.load("BorderedBox.png", Texture.class);
        assetManager.load("PlayButton.png", Texture.class);
        assetManager.load("CrateTexture.jpg", Texture.class);
        assetManager.load("Platform.png", Texture.class);
        assetManager.load("SinglePlatform.png", Texture.class);
        assetManager.load("Hero.png", Texture.class);
        assetManager.load("HeroTextured.png", Texture.class);
        assetManager.load("LeftRightButton.png", Texture.class);
        assetManager.load("JumpButton.png", Texture.class);
        assetManager.finishLoading();
    }

    public Texture GetTextureAsset(String fileName) {
        return assetManager.get(fileName, Texture.class);
    }

    boolean platformClicked;
    boolean canJump;
    Array<Contact> contactList;
    public void Update(OrthographicCamera camera)
    {
        currentlyPressed = Gdx.input.isTouched();
        contactList = world.getContactList();
        playButton.Update();
        leftButton.Update();
        rightButton.Update();
        leftJumpButton.Update();
        rightJumpButton.Update();

        if(playButton.Tapped()) {
            showGrid = !showGrid;
            if(showGrid) {
                hero.setPosition(hero.startingPositionStatic.x, hero.startingPositionStatic.y);
                hero.setRotation(0);
                hero.heroBody.setTransform(hero.startingPositionWorld.x, hero.startingPositionWorld.y, 0);
                hero.heroBody.setLinearVelocity(0, 0);
                hero.heroBody.setAngularVelocity(0);
            }
        }


        platformClicked = false;
        for(PlatformBlock platformBlock : platformBlocks) {
            platformBlock.Update();
            if(platformBlock.Clicked()) {
                platformClicked = true;
            }
        }
        canJump = false;
        for(int i = 0; i < world.getContactList().size; i++) {
            if(contactList.get(i).getFixtureA().getBody() == hero.heroBody || contactList.get(i).getFixtureB().getBody() == hero.heroBody){
                if(contactList.get(i).getFixtureA().getBody() == ground || contactList.get(i).getFixtureB().getBody() == ground) {
                    canJump = true;
                    break;
                }
                for(int j = 0; j < platformBlocks.size(); j++) {
                    if(contactList.get(i).getFixtureA().getBody() == platformBlocks.get(j).platformBody || contactList.get(i).getFixtureB().getBody() == platformBlocks.get(j).platformBody){
                        if(hero.bottomBoundingBox.overlaps(platformBlocks.get(j).topBoundingBox)) {
                            canJump = true;
                            break;
                        }
                    }
                }
                if(canJump){
                    break;
                }
            }
        }
        hero.Update(camera, canJump, leftButton.Pressed(), rightButton.Pressed(), (leftJumpButton.Tapped() || rightJumpButton.Tapped()));

        if(!platformClicked) {
            int index = 0;
            for (Rectangle gridBox : gridBoxes) {
                if (GestureManager.IsCurrentlyBeingTouched(gridBox) && !currentlyPressed && previousPressed) {
                    textLocation = new Vector2(gridBox.x, gridBox.y);
                    rowCol.y = (gridBox.x - viewportDifference / 2f) / (viewport.x / (float) gridSize) + .1f;
                    rowCol.x = gridBox.y / (viewport.x / gridSize);
                    selectedIndex = index;
                    for(PlatformBlock platformBlock : platformBlocks) {
                        if(platformBlock.destroyed) {
                            platformBlock.Transport(gridBox, camera);
                            break;
                        }
                    }
                }
                index++;
            }
        }

        previousPressed = currentlyPressed;
    }

    public void Draw(OrthographicCamera camera) {
        staticBatch.begin();

        if(showGrid) {
            int index = 0;
            for (Rectangle gridBox : gridBoxes) {
                gridSprite.setPosition(gridBox.getX(), gridBox.getY());
                gridSprite.setSize(gridBox.width, gridBox.height);
//            if(index == selectedIndex) {
//                gridSprite.setColor(Color.RED);
//            } else if(gridSprite.getColor() != Color.WHITE) {
//                gridSprite.setColor(Color.WHITE);
//            }
                gridSprite.draw(staticBatch);
                index++;
            }
        }
        for(PlatformBlock platformBlock : platformBlocks) {
            platformBlock.Draw(staticBatch);
        }
        platformSprite.draw(staticBatch);
        playButton.draw(staticBatch);
        hero.draw(staticBatch);

        staticBatch.end();

        rayHandler.setCombinedMatrix(camera.combined);
        rayHandler.updateAndRender();

        staticBatch.begin();
//        font.draw(staticBatch, String.format("Row: %d, Col: %d", (int)rowCol.x, (int)rowCol.y),
//                textLocation.x, textLocation.y + font.getLineHeight());
        font.draw(staticBatch, String.format("FPS: %d", Gdx.graphics.getFramesPerSecond()),
                Gdx.graphics.getWidth() - 350f * scale, Gdx.graphics.getHeight() - 50f * scale);
//        font.draw(staticBatch, String.format("X: %f, Y: %f", GestureManager.InputLocation().x, GestureManager.InputLocation().y), GestureManager.InputLocation().x, GestureManager.InputLocation().y);
        leftButton.draw(staticBatch);
        rightButton.draw(staticBatch);
        leftJumpButton.draw(staticBatch);
        rightJumpButton.draw(staticBatch);
        staticBatch.end();
    }
}
