package com.builditplatformer.game;

import com.badlogic.gdx.*;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class Main extends Game {
    SpriteBatch batch;
    OrthographicCamera camera;
    World world;
    Box2DDebugRenderer debugRenderer;
    float scale;
    Level level01;
    AssetManager assetManager;

    @Override
    public void create() {
        Gdx.graphics.setDisplayMode(1920, 1080, true);
        scale = Gdx.graphics.getWidth() / 1920f;
        batch = new SpriteBatch();

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.zoom = .01f;
        camera.update();

        world = new World(new Vector2(0, -9.8f), false);

        debugRenderer = new Box2DDebugRenderer();

        assetManager = new AssetManager();
        LoadAssets();
        level01 = new Level(world, 16, 6, camera,  scale, assetManager);
    }

    @Override
    public void render() {
        Update();
        Draw();
    }

    public void Update() {
//        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
//            camera.position.y++;
//        }
//        if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
//            camera.zoom+=.02f;
//        }

        level01.Update(camera);
//        camera.position.x = ground.getPosition().x;
//        camera.position.y = ground.getPosition().y;
        camera.update();

        world.step(1/60f, 6, 2);
//        level01.rayHandler.update();
    }

    public void Draw() {
        Gdx.gl.glClearColor(.6f, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.end();

        level01.Draw(camera);
//        level01.rayHandler.setCombinedMatrix(camera.combined);
//        level01.rayHandler.render();
//        level01.rayHandler.updateAndRender();
        debugRenderer.render(world, camera.combined);
    }


    private void LoadAssets() {
        assetManager.load("WhitePixel.png", Texture.class);
        assetManager.finishLoading();
    }

    public Texture GetTextureAsset(String fileName) {
        return assetManager.get(fileName, Texture.class);
    }
}
